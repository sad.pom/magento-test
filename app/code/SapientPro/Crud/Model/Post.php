<?php

namespace SapientPro\Crud\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Post
 * @package SapientPro\Crud\Model
 */
class Post extends AbstractModel implements IdentityInterface
{
    /**
     * Set cache tag
     */
    const CACHE_TAG = 'sapient_crud_post';

    /**
     * @var string
     */
    protected $_cacheTag = 'sapient_crud_post';

    /**
     * @var string
     */
    protected $_eventPrefix = 'sapient_crud_post';

    /**
     * Set resource model
     */
    protected function _construct()
    {
        $this->_init('SapientPro\Crud\Model\ResourceModel\Post');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
