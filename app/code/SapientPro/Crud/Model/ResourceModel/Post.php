<?php


namespace SapientPro\Crud\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Post
 * @package SapientPro\Crud\Model\ResourceModel
 */
class Post extends AbstractDb
{

    /**
     * Post constructor.
     * @param Context $context
     */
    public function __construct(Context $context) {
        parent::__construct($context);
    }

    /**
     * This method define the table name and primary key for that table
     */
    protected function _construct()
    {
        $this->_init('sapient_crud_post', 'post_id');
    }

}
