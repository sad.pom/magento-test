<?php


namespace SapientPro\Crud\Block;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SapientPro\Crud\Model\PostFactory;

class Index extends Template
{
    /**
     * @var PostFactory
     */
    protected $_postFactory;

    /**
     * Display constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     */
    public function __construct(Context $context, PostFactory $postFactory)
    {
        $this->_postFactory = $postFactory;
        parent::__construct($context);
    }

    /**
     * @return Phrase
     */
    public function sayHello()
    {
        return __('Hello World');
    }

    /**
     * @return AbstractCollection
     */
    public function getPostCollection(){
        $post = $this->_postFactory->create();

        return $post->getCollection();
    }
}
