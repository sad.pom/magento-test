<?php


namespace SapientPro\Demo\Helper;

use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\Helper\Data;

class ProductHelper
{
    /**
     * @var Data PriceHelper
     */
    private $priceHelper;

    /**
     * ProductHelper constructor.
     * @param Data $priceHelper
     */
    public function __construct(Data $priceHelper)
    {
        $this->priceHelper = $priceHelper;
    }

    /**
     * @param string $price
     * @return float|string
     */
    public function formatPrice($price)
    {
        return $this->priceHelper->currency($price, true, false);
    }

    /**
     * @param Product $product
     * @return array
     */
    public function geProductImagesArray($product)
    {
        $images = $product->getMediaGalleryImages();
        $imageArray = [];
        foreach ($images as $image) {
            $imageArray[] = $image->getUrl();
        }

        return $imageArray;
    }
}
