<?php

namespace SapientPro\Demo\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use SapientPro\Demo\Api\Data\ProductInterface;

interface ProductRepositoryInterface
{
    /**
     * Get product by his Id
     * @param int $id
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProductById($id);
}
