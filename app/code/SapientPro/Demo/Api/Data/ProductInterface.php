<?php

namespace SapientPro\Demo\Api\Data;

/**
 * Interface ProductInterface
 * @package SapientPro\Demo\Api\Data
 */
interface ProductInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getSku();

    /**
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getPrice();

    /**
     * @param string $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * @return string[]
     */
    public function getImages();

    /**
     * @param string[] $imagesArray
     * @return $this
     */
    public function setImages($imagesArray);

    /**
     * @return string
     */
    public function getSpecialPrice();

    /**
     * @param string $specialPrice
     * @return $this
     */
    public function setSpecialPrice($specialPrice);
}
