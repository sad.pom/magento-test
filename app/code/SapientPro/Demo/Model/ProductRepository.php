<?php

namespace SapientPro\Demo\Model;

use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use SapientPro\Demo\Api\Data\ProductInterface;
use SapientPro\Demo\Api\ProductRepositoryInterface;
use SapientPro\Demo\Helper\ProductHelper;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var ProductInterfaceFactory
     */
    private $productInterfaceFactory;

    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * ProductRepository constructor.
     * @param ProductInterfaceFactory $productInterfaceFactory
     * @param ProductHelper $productHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductInterfaceFactory $productInterfaceFactory,
        ProductHelper $productHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->productInterfaceFactory = $productInterfaceFactory;
        $this->productHelper = $productHelper;
        $this->productRepository = $productRepository;
    }


    /**
     * Get product by his Id
     * @param int $id
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProductById($id)
    {
        /** @var ProductInterface $productInterface */
        $productInterface = $this->productInterfaceFactory->create();
        try {
            /** @var Product $product */
            $product = $this->productRepository->getById($id);
            $productInterface->setId($product->getId());
            $productInterface->setSku($product->getSku());
            $productInterface->setName($product->getName());
            $productInterface->setDescription($product->getDescription() ?? '');
            $productInterface->setPrice($this->productHelper->formatPrice($product->getPrice()));
            $productInterface->setImages($this->productHelper->geProductImagesArray($product));
            $productInterface->setSpecialPrice($this->productHelper->formatPrice($product->getSpecialPrice()));

            return $productInterface;
        } catch (NoSuchEntityException $e) {
            throw NoSuchEntityException::singleField('id', $id);
        }
    }
}
