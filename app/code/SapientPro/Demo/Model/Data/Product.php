<?php

namespace SapientPro\Demo\Model\Data;

use Magento\Framework\DataObject;
use SapientPro\Demo\Api\Data\ProductInterface;

class Product extends DataObject implements ProductInterface
{

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->getData('sku');
    }

    /**
     * @param string $sku
     * @return $this
     */
    public function setSku($sku)
    {
        return $this->setData('sku', $sku);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData('description');
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->getData('price');
    }

    /**
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        return $this->setData('price', $price);
    }

    /**
     * @return string[]
     */
    public function getImages()
    {
        return $this->getData('images');
    }

    /**
     * @param string[] $imagesArray
     * @return $this
     */
    public function setImages($imagesArray)
    {
        return $this->setData('images', $imagesArray);
    }

    /**
     * @return string
     */
    public function getSpecialPrice()
    {
        return $this->getData('special_price');
    }

    /**
     * @param string $specialPrice
     * @return $this
     */
    public function setSpecialPrice($specialPrice)
    {
        return $this->setData('special_price', $specialPrice);
    }
}
