<?php

namespace SapientPro\HelloPage\Block;

use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class View
 * @package SapientPro\Crud\Block
 */
class View extends Template
{
    /**
     * Display constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
        die('$');
    }

    /**
     * @return Phrase
     */
    public function sayHello()
    {
        return __('Hello World');
    }
}
